<?php

/*
 * This file is part of the Bos Oanda Client Bundle.
 * (c) Michael A. Bos <mab05k@gmail.com>
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Bos\OandaClient\Bridge\Symfony\Bundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

/**
 * Class BosOandaClientBundle.
 */
class BosOandaClientBundle extends Bundle
{
}
