<?php

/*
 * This file is part of the Bos Oanda Client Bundle.
 * (c) Michael A. Bos <mab05k@gmail.com>
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Bos\OandaClient\Response\Account;

use Bos\OandaClient\Definition\Account\Instrument;
use Bos\OandaClient\Definition\Traits\LastTransactionIdTrait;
use JMS\Serializer\Annotation as Serializer;

/**
 * Class InstrumentResponse.
 */
class InstrumentResponse
{
    use LastTransactionIdTrait;

    /**
     * @var array|Instrument[]
     *
     * @Serializer\SerializedName("instruments")
     * @Serializer\Type("array<Bos\OandaClient\Definition\Account\Instrument>")
     */
    private $instruments = [];

    /**
     * @return array|Instrument[]
     */
    public function getInstruments()
    {
        return $this->instruments;
    }

    /**
     * @param array|Instrument[] $instruments
     *
     * @return InstrumentResponse
     */
    public function setInstruments($instruments)
    {
        $this->instruments = $instruments;

        return $this;
    }
}
