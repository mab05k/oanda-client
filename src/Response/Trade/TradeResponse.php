<?php

/*
 * This file is part of the Bos Oanda Client Bundle.
 * (c) Michael A. Bos <mab05k@gmail.com>
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Bos\OandaClient\Response\Trade;

use Bos\OandaClient\Definition\Trade\Trade;
use Bos\OandaClient\Definition\Traits\LastTransactionIdTrait;
use JMS\Serializer\Annotation as Serializer;

/**
 * Class TradeResponse.
 */
class TradeResponse
{
    use LastTransactionIdTrait;

    /**
     * @var Trade|null
     *
     * @Serializer\SerializedName("trade")
     * @Serializer\Type("Bos\OandaClient\Definition\Trade\Trade")
     */
    private $trade;

    /**
     * @return Trade|null
     */
    public function getTrade(): ?Trade
    {
        return $this->trade;
    }

    /**
     * @param Trade|null $trade
     *
     * @return TradeResponse
     */
    public function setTrade(?Trade $trade): self
    {
        $this->trade = $trade;

        return $this;
    }
}
