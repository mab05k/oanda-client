<?php

/*
 * This file is part of the Bos Oanda Client Bundle.
 * (c) Michael A. Bos <mab05k@gmail.com>
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Bos\OandaClient\Response\Position;

use Bos\OandaClient\Definition\Position\Position;
use Bos\OandaClient\Definition\Traits\LastTransactionIdTrait;
use JMS\Serializer\Annotation as Serializer;

class PositionResponse
{
    use LastTransactionIdTrait;

    /**
     * @var Position|null
     *
     * @Serializer\SerializedName("position")
     * @Serializer\Type("Bos\OandaClient\Definition\Position\Position")
     */
    private $position;

    /**
     * @return Position|null
     */
    public function getPosition(): ?Position
    {
        return $this->position;
    }

    /**
     * @param Position|null $position
     *
     * @return PositionResponse
     */
    public function setPosition(?Position $position): self
    {
        $this->position = $position;

        return $this;
    }
}
