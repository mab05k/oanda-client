<?php

/*
 * This file is part of the Bos Oanda Client Bundle.
 * (c) Michael A. Bos <mab05k@gmail.com>
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Bos\OandaClient\Request\Query\Order;

use Bos\OandaClient\Request\Query\AbstractQueryParameter;
use Bos\OandaClient\Request\Query\QueryParameterInterface;

/**
 * Class State.
 */
class State extends AbstractQueryParameter implements QueryParameterInterface
{
    public const KEY = 'state';

    const PENDING = 'PENDING';
    const FILLED = 'FILLED';
    const TRIGGERED = 'TRIGGERED';
    const CANCELLED = 'CANCELLED';
    const ALL = 'ALL';
}
