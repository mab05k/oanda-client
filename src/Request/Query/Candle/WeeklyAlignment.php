<?php

/*
 * This file is part of the Bos Oanda Client Bundle.
 * (c) Michael A. Bos <mab05k@gmail.com>
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Bos\OandaClient\Request\Query\Candle;

use Bos\OandaClient\Request\Query\AbstractQueryParameter;
use Bos\OandaClient\Request\Query\QueryParameterInterface;

/**
 * Class WeeklyAlignment.
 *
 * @author Michael Bos <mab05k@gmail.com>
 */
class WeeklyAlignment extends AbstractQueryParameter implements QueryParameterInterface
{
    public const KEY = 'weeklyAlignment';

    public const MONDAY = 'Monday';
    public const TUESDAY = 'Tuesday';
    public const WEDNESDAY = 'Wednesday';
    public const THURSDAY = 'Thursday';
    public const FRIDAY = 'Friday';
    public const SATURDAY = 'Saturday';
    public const SUNDAY = 'Sunday';
}
