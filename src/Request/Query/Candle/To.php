<?php

/*
 * This file is part of the Bos Oanda Client Bundle.
 * (c) Michael A. Bos <mab05k@gmail.com>
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Bos\OandaClient\Request\Query\Candle;

use Bos\OandaClient\Request\Query\AbstractDateTimeQueryParameter;
use Bos\OandaClient\Request\Query\QueryParameterInterface;

/**
 * Class To.
 *
 * @author Michael Bos <mab05k@gmail.com>
 */
class To extends AbstractDateTimeQueryParameter implements QueryParameterInterface
{
    public const KEY = 'to';
}
