<?php

/*
 * This file is part of the Bos Oanda Client Bundle.
 * (c) Michael A. Bos <mab05k@gmail.com>
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Bos\OandaClient\Request;

use Bos\OandaClient\Definition\Transaction\ClientExtension\ClientExtension;
use Bos\OandaClient\Request\Position\ClosePositionRequest;

/**
 * Class PositionRequestFactory.
 */
class PositionRequestFactory
{
    /**
     * @param string               $longUnits
     * @param string               $shortUnits
     * @param ClientExtension|null $longClientExtensions
     * @param ClientExtension|null $shortClientExtensions
     *
     * @return ClosePositionRequest
     */
    public static function closePositionRequest(
        string $longUnits,
        string $shortUnits,
        ?ClientExtension $longClientExtensions = null,
        ?ClientExtension $shortClientExtensions = null
    ): ClosePositionRequest {
        return (new ClosePositionRequest())
            ->setLongUnits($longUnits)
            ->setShortUnits($shortUnits)
            ->setLongClientExtensions($longClientExtensions)
            ->setShortClientExtensions($shortClientExtensions);
    }
}
