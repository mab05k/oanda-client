<?php

/*
 * This file is part of the Bos Oanda Client Bundle.
 * (c) Michael A. Bos <mab05k@gmail.com>
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Bos\OandaClient\Request;

use Bos\OandaClient\Definition\Account\Configuration;
use Brick\Math\BigDecimal;

/**
 * Class AccountRequestFactory.
 */
class AccountRequestFactory
{
    /**
     * @param string     $alias
     * @param BigDecimal $marginRate
     *
     * @return Configuration
     */
    public static function configuration(
        ?string $alias = null,
        ?BigDecimal $marginRate = null
    ): Configuration {
        return (new Configuration())
            ->setMarginRate($marginRate)
            ->setAlias($alias);
    }
}
