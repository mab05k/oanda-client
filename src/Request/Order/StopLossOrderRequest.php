<?php

/*
 * This file is part of the Bos Oanda Client Bundle.
 * (c) Michael A. Bos <mab05k@gmail.com>
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Bos\OandaClient\Request\Order;

use Bos\OandaClient\Definition\Traits\DistanceTrait;
use JMS\Serializer\Annotation as Serializer;

/**
 * Class StopLossOrderRequest.
 *
 * @author Michael Bos <mab05k@gmail.com>
 */
class StopLossOrderRequest extends AbstractTradeOrderRequest
{
    use DistanceTrait;

    /**
     * @var bool|null
     *
     * @Serializer\SerializedName("guaranteed")
     * @Serializer\Type("boolean")
     */
    private $guaranteed;

    /**
     * @return bool|null
     */
    public function getGuaranteed(): ?bool
    {
        return $this->guaranteed;
    }

    /**
     * @param bool|null $guaranteed
     *
     * @return StopLossOrderRequest
     */
    public function setGuaranteed(?bool $guaranteed): self
    {
        $this->guaranteed = $guaranteed;

        return $this;
    }
}
