<?php

/*
 * This file is part of the Bos Oanda Client Bundle.
 * (c) Michael A. Bos <mab05k@gmail.com>
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Bos\OandaClient\Request\Order\Envelope;

use Bos\OandaClient\Request\Order\MarketIfTouchedOrderRequest;
use JMS\Serializer\Annotation as Serializer;

/**
 * Class MarketIfTouchedOrderRequest.
 */
class MarketIfTouchedOrderRequestEnvelope implements OrderInterface
{
    /**
     * @var MarketIfTouchedOrderRequest|null
     *
     * @Serializer\SerializedName("order")
     * @Serializer\Type("Bos\OandaClient\Request\Order\MarketIfTouchedOrderRequest")
     */
    private $order;

    /**
     * @return MarketIfTouchedOrderRequest|null
     */
    public function getOrder(): ?MarketIfTouchedOrderRequest
    {
        return $this->order;
    }

    /**
     * @param MarketIfTouchedOrderRequest|null $order
     *
     * @return MarketIfTouchedOrderRequestEnvelope
     */
    public function setOrder(?MarketIfTouchedOrderRequest $order): self
    {
        $this->order = $order;

        return $this;
    }
}
