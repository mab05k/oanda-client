<?php

/*
 * This file is part of the Bos Oanda Client Bundle.
 * (c) Michael A. Bos <mab05k@gmail.com>
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Bos\OandaClient\Request\Order\Envelope;

use Bos\OandaClient\Request\Order\StopOrderRequest;
use JMS\Serializer\Annotation as Serializer;

/**
 * Class StopOrderRequestEnvelope.
 */
class StopOrderRequestEnvelope implements OrderInterface
{
    /**
     * @var StopOrderRequest|null
     *
     * @Serializer\SerializedName("order")
     * @Serializer\Type("Bos\OandaClient\Request\Order\StopOrderRequest")
     */
    private $order;

    /**
     * @return StopOrderRequest|null
     */
    public function getOrder(): ?StopOrderRequest
    {
        return $this->order;
    }

    /**
     * @param StopOrderRequest|null $order
     *
     * @return StopOrderRequestEnvelope
     */
    public function setOrder(?StopOrderRequest $order): self
    {
        $this->order = $order;

        return $this;
    }
}
