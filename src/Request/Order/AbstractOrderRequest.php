<?php

/*
 * This file is part of the Bos Oanda Client Bundle.
 * (c) Michael A. Bos <mab05k@gmail.com>
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Bos\OandaClient\Request\Order;

use Bos\OandaClient\Definition\Traits\InstrumentTrait;
use Bos\OandaClient\Definition\Traits\StopLossOnFillTrait;
use Bos\OandaClient\Definition\Traits\TakeProfitOnFillTrait;
use Bos\OandaClient\Definition\Traits\TimeInForceTrait;
use Bos\OandaClient\Definition\Traits\TradeClientExtensionsTrait;
use Bos\OandaClient\Definition\Traits\TrailingStopLossOnFillTrait;
use Bos\OandaClient\Definition\Traits\TypeTrait;
use Bos\OandaClient\Definition\Traits\UnitTrait;

/**
 * Class AbstractOrderRequest.
 */
abstract class AbstractOrderRequest
{
    use TypeTrait;
    use InstrumentTrait;
    use UnitTrait;
    use TimeInForceTrait;
    use TakeProfitOnFillTrait;
    use StopLossOnFillTrait;
    use TrailingStopLossOnFillTrait;
    use TradeClientExtensionsTrait;
}
