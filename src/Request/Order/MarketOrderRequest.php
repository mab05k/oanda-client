<?php

/*
 * This file is part of the Bos Oanda Client Bundle.
 * (c) Michael A. Bos <mab05k@gmail.com>
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Bos\OandaClient\Request\Order;

use Bos\OandaClient\Definition\Traits\ClientExtensionsTrait;
use Bos\OandaClient\Definition\Traits\PositionFillTrait;
use Bos\OandaClient\Definition\Traits\PriceBoundTrait;

/**
 * Class MarketOrderRequest.
 */
class MarketOrderRequest extends AbstractOrderRequest
{
    use PositionFillTrait;
    use ClientExtensionsTrait;
    use PriceBoundTrait;
}
