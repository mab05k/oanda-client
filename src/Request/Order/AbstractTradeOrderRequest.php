<?php

/*
 * This file is part of the Bos Oanda Client Bundle.
 * (c) Michael A. Bos <mab05k@gmail.com>
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Bos\OandaClient\Request\Order;

use Bos\OandaClient\Definition\Traits\ClientExtensionsTrait;
use Bos\OandaClient\Definition\Traits\ClientTradeIdTrait;
use Bos\OandaClient\Definition\Traits\GoodUntilDateTrait;
use Bos\OandaClient\Definition\Traits\PriceTrait;
use Bos\OandaClient\Definition\Traits\TimeInForceTrait;
use Bos\OandaClient\Definition\Traits\TradeIdTrait;
use Bos\OandaClient\Definition\Traits\TriggerConditionTrait;
use Bos\OandaClient\Definition\Traits\TypeTrait;

/**
 * Class AbstractTradeOrderRequest.
 */
class AbstractTradeOrderRequest
{
    use TypeTrait;
    use TradeIdTrait;
    use ClientTradeIdTrait;
    use PriceTrait;
    use TimeInForceTrait;
    use GoodUntilDateTrait;
    use TriggerConditionTrait;
    use ClientExtensionsTrait;
}
