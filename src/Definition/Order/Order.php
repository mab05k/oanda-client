<?php

/*
 * This file is part of the Bos Oanda Client Bundle.
 * (c) Michael A. Bos <mab05k@gmail.com>
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Bos\OandaClient\Definition\Order;

use Bos\OandaClient\Definition\Traits\ClientExtensionsTrait;
use Bos\OandaClient\Definition\Traits\CreateTimeTrait;
use Bos\OandaClient\Definition\Traits\FilledOrderTrait;
use Bos\OandaClient\Definition\Traits\IdTrait;
use Bos\OandaClient\Definition\Traits\InstrumentTrait;
use Bos\OandaClient\Definition\Traits\PartialFillTrait;
use Bos\OandaClient\Definition\Traits\PositionFillTrait;
use Bos\OandaClient\Definition\Traits\PriceTrait;
use Bos\OandaClient\Definition\Traits\StateTrait;
use Bos\OandaClient\Definition\Traits\TimeInForceTrait;
use Bos\OandaClient\Definition\Traits\TradeIdTrait;
use Bos\OandaClient\Definition\Traits\TradeStatusIdTrait;
use Bos\OandaClient\Definition\Traits\TriggerConditionTrait;
use Bos\OandaClient\Definition\Traits\TypeTrait;
use Bos\OandaClient\Definition\Traits\UnitTrait;
use JMS\Serializer\Annotation as Serializer;

/**
 * Class Order.
 */
class Order
{
    use IdTrait;
    use CreateTimeTrait;
    use TradeIdTrait;
    use StateTrait;
    use ClientExtensionsTrait;
    use TypeTrait;
    use InstrumentTrait;
    use UnitTrait;
    use TimeInForceTrait;
    use PositionFillTrait;
    use FilledOrderTrait;
    use PriceTrait;
    use PartialFillTrait;
    use TriggerConditionTrait;
    use TradeStatusIdTrait;

    /**
     * @var bool|null
     *
     * @Serializer\SerializedName("guaranteed")
     * @Serializer\Type("boolean")
     */
    private $guaranteed;

    /**
     * @return bool|null
     */
    public function getGuaranteed(): ?bool
    {
        return $this->guaranteed;
    }

    /**
     * @param bool|null $guaranteed
     *
     * @return Order
     */
    public function setGuaranteed(?bool $guaranteed): self
    {
        $this->guaranteed = $guaranteed;

        return $this;
    }
}
