<?php

/*
 * This file is part of the Bos Oanda Client Bundle.
 * (c) Michael A. Bos <mab05k@gmail.com>
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Bos\OandaClient\Definition\Order;

use Bos\OandaClient\Definition\Traits\CancelledOrderTrait;
use Bos\OandaClient\Definition\Traits\CreateTimeTrait;
use Bos\OandaClient\Definition\Traits\FilledOrderTrait;
use Bos\OandaClient\Definition\Traits\IdTrait;
use Bos\OandaClient\Definition\Traits\ReplacedOrderTrait;
use Bos\OandaClient\Definition\Traits\StateTrait;
use Bos\OandaClient\Definition\Traits\TradeStatusIdTrait;
use Bos\OandaClient\Request\Order\StopLossOrderRequest;
use Brick\Math\BigDecimal;
use JMS\Serializer\Annotation as Serializer;

/**
 * Class StopLossOrder.
 */
class StopLossOrder extends StopLossOrderRequest
{
    use IdTrait;
    use CreateTimeTrait;
    use StateTrait;
    use FilledOrderTrait;
    use TradeStatusIdTrait;
    use CancelledOrderTrait;
    use ReplacedOrderTrait;

    /**
     * @var BigDecimal|null
     *
     * @Serializer\SerializedName("guaranteedExecutionPremium")
     * @Serializer\Type("Brick\Math\BigDecimal")
     */
    private $guaranteedExecutionPremium;

    /**
     * @return BigDecimal|null
     */
    public function getGuaranteedExecutionPremium(): ?BigDecimal
    {
        return $this->guaranteedExecutionPremium;
    }

    /**
     * @param BigDecimal|null $guaranteedExecutionPremium
     *
     * @return StopLossOrder
     */
    public function setGuaranteedExecutionPremium(?BigDecimal $guaranteedExecutionPremium): self
    {
        $this->guaranteedExecutionPremium = $guaranteedExecutionPremium;

        return $this;
    }
}
