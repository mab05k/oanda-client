<?php

/*
 * This file is part of the Bos Oanda Client Bundle.
 * (c) Michael A. Bos <mab05k@gmail.com>
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Bos\OandaClient\Definition\Order;

use Bos\OandaClient\Definition\Traits\CancelledOrderTrait;
use Bos\OandaClient\Definition\Traits\CreateTimeTrait;
use Bos\OandaClient\Definition\Traits\FilledOrderTrait;
use Bos\OandaClient\Definition\Traits\IdTrait;
use Bos\OandaClient\Definition\Traits\ReplacedOrderTrait;
use Bos\OandaClient\Definition\Traits\StateTrait;
use Bos\OandaClient\Definition\Traits\StopLossOnFillTrait;
use Bos\OandaClient\Definition\Traits\TakeProfitOnFillTrait;
use Bos\OandaClient\Definition\Traits\TradeClientExtensionsTrait;
use Bos\OandaClient\Definition\Traits\TradeStatusIdTrait;
use Bos\OandaClient\Definition\Traits\TrailingStopLossOnFillTrait;
use Bos\OandaClient\Request\Order\LimitOrderRequest;

/**
 * Class LimitOrder.
 */
class LimitOrder extends LimitOrderRequest
{
    use IdTrait;
    use CreateTimeTrait;
    use StateTrait;
    use TakeProfitOnFillTrait;
    use StopLossOnFillTrait;
    use TrailingStopLossOnFillTrait;
    use TradeClientExtensionsTrait;
    use FilledOrderTrait;
    use TradeStatusIdTrait;
    use CancelledOrderTrait;
    use ReplacedOrderTrait;
}
