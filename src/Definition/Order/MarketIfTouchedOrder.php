<?php

/*
 * This file is part of the Bos Oanda Client Bundle.
 * (c) Michael A. Bos <mab05k@gmail.com>
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Bos\OandaClient\Definition\Order;

use Bos\OandaClient\Definition\Traits\CancelledOrderTrait;
use Bos\OandaClient\Definition\Traits\CreateTimeTrait;
use Bos\OandaClient\Definition\Traits\FilledOrderTrait;
use Bos\OandaClient\Definition\Traits\IdTrait;
use Bos\OandaClient\Definition\Traits\ReplacedOrderTrait;
use Bos\OandaClient\Definition\Traits\StateTrait;
use Bos\OandaClient\Definition\Traits\TradeStatusIdTrait;
use Bos\OandaClient\Request\Order\MarketIfTouchedOrderRequest;
use Brick\Money\Money;
use JMS\Serializer\Annotation as Serializer;

/**
 * Class MarketIfTouchedOrder.
 *
 * @author Michael Bos <mab05k@gmail.com>
 */
class MarketIfTouchedOrder extends MarketIfTouchedOrderRequest
{
    use IdTrait;
    use CreateTimeTrait;
    use StateTrait;
    use FilledOrderTrait;
    use TradeStatusIdTrait;
    use CancelledOrderTrait;
    use ReplacedOrderTrait;

    /**
     * @var Money|null
     *
     * @Serializer\SerializedName("initialMarketPrice")
     * @Serializer\Type("Brick\Money\Money")
     */
    private $initialMarketPrice;

    /**
     * @return Money|null
     */
    public function getInitialMarketPrice(): ?Money
    {
        return $this->initialMarketPrice;
    }

    /**
     * @param Money|null $initialMarketPrice
     *
     * @return MarketIfTouchedOrder
     */
    public function setInitialMarketPrice(?Money $initialMarketPrice): self
    {
        $this->initialMarketPrice = $initialMarketPrice;

        return $this;
    }
}
