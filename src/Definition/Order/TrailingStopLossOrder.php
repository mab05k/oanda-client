<?php

/*
 * This file is part of the Bos Oanda Client Bundle.
 * (c) Michael A. Bos <mab05k@gmail.com>
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Bos\OandaClient\Definition\Order;

use Bos\OandaClient\Definition\Traits\CancelledOrderTrait;
use Bos\OandaClient\Definition\Traits\CreateTimeTrait;
use Bos\OandaClient\Definition\Traits\FilledOrderTrait;
use Bos\OandaClient\Definition\Traits\IdTrait;
use Bos\OandaClient\Definition\Traits\ReplacedOrderTrait;
use Bos\OandaClient\Definition\Traits\StateTrait;
use Bos\OandaClient\Definition\Traits\TradeStatusIdTrait;
use Bos\OandaClient\Request\Order\TrailingStopLossOrderRequest;
use Brick\Money\Money;

/**
 * Class TrailingStopLossOrder.
 */
class TrailingStopLossOrder extends TrailingStopLossOrderRequest
{
    use IdTrait;
    use CreateTimeTrait;
    use StateTrait;
    use FilledOrderTrait;
    use TradeStatusIdTrait;
    use CancelledOrderTrait;
    use ReplacedOrderTrait;

    /**
     * @var Money|null
     *
     * @Serializer\SerializedName("trailingStopValue")
     * @Serializer\Type("Brick\Money\Money")
     */
    private $trailingStopValue;

    /**
     * @return Money|null
     */
    public function getTrailingStopValue(): ?Money
    {
        return $this->trailingStopValue;
    }

    /**
     * @param Money|null $trailingStopValue
     *
     * @return TrailingStopLossOrder
     */
    public function setTrailingStopValue(?Money $trailingStopValue): self
    {
        $this->trailingStopValue = $trailingStopValue;

        return $this;
    }
}
