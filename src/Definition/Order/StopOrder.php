<?php

/*
 * This file is part of the Bos Oanda Client Bundle.
 * (c) Michael A. Bos <mab05k@gmail.com>
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Bos\OandaClient\Definition\Order;

use Bos\OandaClient\Definition\Traits\CancelledOrderTrait;
use Bos\OandaClient\Definition\Traits\CreateTimeTrait;
use Bos\OandaClient\Definition\Traits\FilledOrderTrait;
use Bos\OandaClient\Definition\Traits\IdTrait;
use Bos\OandaClient\Definition\Traits\StateTrait;
use Bos\OandaClient\Definition\Traits\TradeStatusIdTrait;
use Bos\OandaClient\Request\Order\StopOrderRequest;

/**
 * Class StopOrder.
 */
class StopOrder extends StopOrderRequest
{
    use IdTrait;
    use CreateTimeTrait;
    use StateTrait;
    use FilledOrderTrait;
    use TradeStatusIdTrait;
    use CancelledOrderTrait;
}
