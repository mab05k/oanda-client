<?php

/*
 * This file is part of the Bos Oanda Client Bundle.
 * (c) Michael A. Bos <mab05k@gmail.com>
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Bos\OandaClient\Definition\Pricing\Price;

/**
 * Class Ask.
 *
 * @author Michael Bos <mab05k@gmail.com>
 */
class Ask extends AbstractPrice
{
    /**
     * @return string
     */
    public function getType(): string
    {
        return 'ask';
    }
}
