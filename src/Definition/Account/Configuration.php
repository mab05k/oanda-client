<?php

/*
 * This file is part of the Bos Oanda Client Bundle.
 * (c) Michael A. Bos <mab05k@gmail.com>
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Bos\OandaClient\Definition\Account;

use Bos\OandaClient\Definition\Traits\AliasTrait;
use Brick\Math\BigDecimal;
use JMS\Serializer\Annotation as Serializer;

/**
 * Class Configuration.
 */
class Configuration
{
    use AliasTrait;

    /**
     * @var BigDecimal|null
     *
     * @Serializer\SerializedName("marginRate")
     * @Serializer\Type("Brick\Math\BigDecimal")
     */
    private $marginRate;

    /**
     * @return BigDecimal|null
     */
    public function getMarginRate(): ?BigDecimal
    {
        return $this->marginRate;
    }

    /**
     * @param BigDecimal|null $marginRate
     *
     * @return Configuration
     */
    public function setMarginRate(?BigDecimal $marginRate): self
    {
        $this->marginRate = $marginRate;

        return $this;
    }
}
