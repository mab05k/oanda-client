<?php

/*
 * This file is part of the Bos Oanda Client Bundle.
 * (c) Michael A. Bos <mab05k@gmail.com>
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Bos\OandaClient\Definition\Transaction\Order;

use Bos\OandaClient\Definition\Traits\OrderTransactionTrait;
use Bos\OandaClient\Definition\Traits\RejectReasonTrait;
use Bos\OandaClient\Definition\Traits\TransactionOrderIdTrait;
use Bos\OandaClient\Definition\Traits\TypeTrait;

/**
 * Class OrderCancelRejectTransaction.
 */
class OrderCancelRejectTransaction
{
    use RejectReasonTrait;
    use TypeTrait;
    use TransactionOrderIdTrait;
    use OrderTransactionTrait;
}
