<?php

/*
 * This file is part of the Bos Oanda Client Bundle.
 * (c) Michael A. Bos <mab05k@gmail.com>
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Bos\OandaClient\Definition\Transaction\Order;

use Bos\OandaClient\Definition\Traits\ClientExtensionsTrait;
use Bos\OandaClient\Definition\Traits\InstrumentTrait;
use Bos\OandaClient\Definition\Traits\OrderTransactionTrait;
use Bos\OandaClient\Definition\Traits\PartialFillTrait;
use Bos\OandaClient\Definition\Traits\PositionFillTrait;
use Bos\OandaClient\Definition\Traits\PriceBoundTrait;
use Bos\OandaClient\Definition\Traits\PriceTrait;
use Bos\OandaClient\Definition\Traits\ReasonTrait;
use Bos\OandaClient\Definition\Traits\RejectReasonTrait;
use Bos\OandaClient\Definition\Traits\TimeInForceTrait;
use Bos\OandaClient\Definition\Traits\TradeCloseTrait;
use Bos\OandaClient\Definition\Traits\TriggerConditionTrait;
use Bos\OandaClient\Definition\Traits\TypeTrait;
use Bos\OandaClient\Definition\Traits\UnitTrait;

/**
 * Class OrderRejectTransaction.
 */
class OrderRejectTransaction implements OrderTransactionInterface
{
    use TypeTrait;
    use UnitTrait;
    use PriceTrait;
    use PriceBoundTrait;
    use InstrumentTrait;
    use TimeInForceTrait;
    use PositionFillTrait;
    use ClientExtensionsTrait;
    use OrderTransactionTrait;
    use ReasonTrait;
    use TradeCloseTrait;
    use TriggerConditionTrait;
    use PartialFillTrait;
    use RejectReasonTrait;
}
