<?php

/*
 * This file is part of the Bos Oanda Client Bundle.
 * (c) Michael A. Bos <mab05k@gmail.com>
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Bos\OandaClient\Definition\Transaction\Order;

use Bos\OandaClient\Definition\Traits\CommissionTrait;
use Bos\OandaClient\Definition\Traits\FinancingTrait;
use Bos\OandaClient\Definition\Traits\FullPriceTrait;
use Bos\OandaClient\Definition\Traits\HalfSpreadCostTrait;
use Bos\OandaClient\Definition\Traits\InstrumentTrait;
use Bos\OandaClient\Definition\Traits\OrderTransactionTrait;
use Bos\OandaClient\Definition\Traits\PriceTrait;
use Bos\OandaClient\Definition\Traits\ProfitLossTrait;
use Bos\OandaClient\Definition\Traits\ReasonTrait;
use Bos\OandaClient\Definition\Traits\TradeOpenedTrait;
use Bos\OandaClient\Definition\Traits\TradesClosedTrait;
use Bos\OandaClient\Definition\Traits\TransactionOrderIdTrait;
use Bos\OandaClient\Definition\Traits\TypeTrait;
use Bos\OandaClient\Definition\Traits\UnitTrait;
use Brick\Math\BigDecimal;
use Brick\Money\Money;
use JMS\Serializer\Annotation as Serializer;

/**
 * Class OrderFillTransaction.
 */
class OrderFillTransaction implements OrderTransactionInterface
{
    use TypeTrait;
    use InstrumentTrait;
    use UnitTrait;
    use PriceTrait;
    use OrderTransactionTrait;
    use ReasonTrait;
    use TransactionOrderIdTrait;
    use FullPriceTrait;
    use TradeOpenedTrait;
    use HalfSpreadCostTrait;
    use FinancingTrait;
    use CommissionTrait;
    use TradesClosedTrait;
    use ProfitLossTrait;

    /**
     * @var Money|null
     *
     * @Serializer\SerializedName("accountBalance")
     * @Serializer\Type("Brick\Money\Money")
     */
    private $accountBalance;

    /**
     * @var BigDecimal|null
     *
     * @Serializer\SerializedName("gainQuoteHomeConversionFactor")
     * @Serializer\Type("Brick\Math\BigDecimal")
     */
    private $gainQuoteHomeConversionFactor;

    /**
     * @var BigDecimal|null
     *
     * @Serializer\SerializedName("lossQuoteHomeConversionFactor")
     * @Serializer\Type("Brick\Math\BigDecimal")
     */
    private $lossQuoteHomeConversionFactor;

    /**
     * @var Money|null
     *
     * @Serializer\SerializedName("guaranteedExecutionFee")
     * @Serializer\Type("Brick\Money\Money")
     */
    private $guaranteedExecutionFee;

    /**
     * @var BigDecimal|null
     *
     * @Serializer\SerializedName("requestedUnits")
     * @Serializer\Type("Brick\Math\BigDecimal")
     */
    private $requestedUnits;

    /**
     * @var Money|null
     *
     * @Serializer\SerializedName("fullVWAP")
     * @Serializer\Type("Brick\Money\Money")
     */
    private $fullVWap;

    /**
     * @return Money|null
     */
    public function getAccountBalance(): ?Money
    {
        return $this->accountBalance;
    }

    /**
     * @param Money|null $accountBalance
     *
     * @return OrderFillTransaction
     */
    public function setAccountBalance(?Money $accountBalance): self
    {
        $this->accountBalance = $accountBalance;

        return $this;
    }

    /**
     * @return BigDecimal|null
     */
    public function getGainQuoteHomeConversionFactor(): ?BigDecimal
    {
        return $this->gainQuoteHomeConversionFactor;
    }

    /**
     * @param BigDecimal|null $gainQuoteHomeConversionFactor
     *
     * @return OrderFillTransaction
     */
    public function setGainQuoteHomeConversionFactor(?BigDecimal $gainQuoteHomeConversionFactor): self
    {
        $this->gainQuoteHomeConversionFactor = $gainQuoteHomeConversionFactor;

        return $this;
    }

    /**
     * @return BigDecimal|null
     */
    public function getLossQuoteHomeConversionFactor(): ?BigDecimal
    {
        return $this->lossQuoteHomeConversionFactor;
    }

    /**
     * @param BigDecimal|null $lossQuoteHomeConversionFactor
     *
     * @return OrderFillTransaction
     */
    public function setLossQuoteHomeConversionFactor(?BigDecimal $lossQuoteHomeConversionFactor): self
    {
        $this->lossQuoteHomeConversionFactor = $lossQuoteHomeConversionFactor;

        return $this;
    }

    /**
     * @return Money|null
     */
    public function getGuaranteedExecutionFee(): ?Money
    {
        return $this->guaranteedExecutionFee;
    }

    /**
     * @param Money|null $guaranteedExecutionFee
     *
     * @return OrderFillTransaction
     */
    public function setGuaranteedExecutionFee(?Money $guaranteedExecutionFee): self
    {
        $this->guaranteedExecutionFee = $guaranteedExecutionFee;

        return $this;
    }

    /**
     * @return BigDecimal|null
     */
    public function getRequestedUnits(): ?BigDecimal
    {
        return $this->requestedUnits;
    }

    /**
     * @param BigDecimal|null $requestedUnits
     *
     * @return OrderFillTransaction
     */
    public function setRequestedUnits(?BigDecimal $requestedUnits): self
    {
        $this->requestedUnits = $requestedUnits;

        return $this;
    }

    /**
     * @return Money|null
     */
    public function getFullVWap(): ?Money
    {
        return $this->fullVWap;
    }

    /**
     * @param Money|null $fullVWap
     *
     * @return OrderFillTransaction
     */
    public function setFullVWap(?Money $fullVWap): self
    {
        $this->fullVWap = $fullVWap;

        return $this;
    }
}
