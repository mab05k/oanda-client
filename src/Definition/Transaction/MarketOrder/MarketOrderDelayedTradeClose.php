<?php

/*
 * This file is part of the Bos Oanda Client Bundle.
 * (c) Michael A. Bos <mab05k@gmail.com>
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Bos\OandaClient\Definition\Transaction\MarketOrder;

use Bos\OandaClient\Definition\Traits\ClientTradeIdTrait;
use Bos\OandaClient\Definition\Traits\TradeIdTrait;

/**
 * Class MarketOrderDelayedTradeClose.
 *
 * @author Michael Bos <mab05k@gmail.com>
 */
class MarketOrderDelayedTradeClose
{
    use TradeIdTrait;
    use ClientTradeIdTrait;

    /**
     * @var string|null
     */
    private $sourceTransactionId;

    /**
     * @return string|null
     */
    public function getSourceTransactionId(): ?string
    {
        return $this->sourceTransactionId;
    }

    /**
     * @param string|null $sourceTransactionId
     *
     * @return MarketOrderDelayedTradeClose
     */
    public function setSourceTransactionId(?string $sourceTransactionId): self
    {
        $this->sourceTransactionId = $sourceTransactionId;

        return $this;
    }
}
