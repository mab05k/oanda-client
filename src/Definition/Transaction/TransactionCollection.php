<?php

/*
 * This file is part of the Bos Oanda Client Bundle.
 * (c) Michael A. Bos <mab05k@gmail.com>
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Bos\OandaClient\Definition\Transaction;

use Bos\OandaClient\Definition\Traits\LastTransactionIdTrait;
use JMS\Serializer\Annotation as Serializer;

/**
 * Class TransactionCollection.
 */
class TransactionCollection
{
    use LastTransactionIdTrait;

    /**
     * @var array|null
     *
     * @Serializer\SerializedName("transactions")
     * @Serializer\Type("array<Bos\OandaClient\Definition\Transaction\Transaction>")
     */
    private $transactions;

    /**
     * @return array|null
     */
    public function getTransactions(): ?array
    {
        return $this->transactions;
    }

    /**
     * @param array|null $transactions
     *
     * @return TransactionCollection
     */
    public function setTransactions(?array $transactions): self
    {
        $this->transactions = $transactions;

        return $this;
    }
}
