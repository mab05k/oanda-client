<?php

/*
 * This file is part of the Bos Oanda Client Bundle.
 * (c) Michael A. Bos <mab05k@gmail.com>
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Bos\OandaClient\Definition\Transaction\Detail;

use Bos\OandaClient\Definition\Traits\ClientExtensionsTrait;
use Bos\OandaClient\Definition\Traits\DistanceTrait;
use Bos\OandaClient\Definition\Traits\GoodUntilDateTrait;
use Bos\OandaClient\Definition\Traits\PriceTrait;
use Bos\OandaClient\Definition\Traits\TimeInForceTrait;
use JMS\Serializer\Annotation as Serializer;

/**
 * Class StopLossDetail.
 *
 * @author Michael Bos <mab05k@gmail.com>
 */
class StopLossDetail
{
    use TimeInForceTrait;
    use PriceTrait;
    use GoodUntilDateTrait;
    use ClientExtensionsTrait;
    use DistanceTrait;

    /**
     * @var bool|null
     *
     * @Serializer\SerializedName("guaranteed")
     * @Serializer\Type("boolean")
     */
    private $guaranteed;

    /**
     * @return bool|null
     */
    public function getGuaranteed(): ?bool
    {
        return $this->guaranteed;
    }

    /**
     * @param bool|null $guaranteed
     *
     * @return StopLossDetail
     */
    public function setGuaranteed(?bool $guaranteed): self
    {
        $this->guaranteed = $guaranteed;

        return $this;
    }
}
