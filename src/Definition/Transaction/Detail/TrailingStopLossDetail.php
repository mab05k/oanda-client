<?php

/*
 * This file is part of the Bos Oanda Client Bundle.
 * (c) Michael A. Bos <mab05k@gmail.com>
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Bos\OandaClient\Definition\Transaction\Detail;

use Bos\OandaClient\Definition\Traits\ClientExtensionsTrait;
use Bos\OandaClient\Definition\Traits\DistanceTrait;
use Bos\OandaClient\Definition\Traits\GoodUntilDateTrait;
use Bos\OandaClient\Definition\Traits\TimeInForceTrait;

/**
 * Class TrailingStopLossDetail.
 */
class TrailingStopLossDetail
{
    use DistanceTrait;
    use TimeInForceTrait;
    use GoodUntilDateTrait;
    use ClientExtensionsTrait;
}
