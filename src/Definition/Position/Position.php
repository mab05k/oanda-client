<?php

/*
 * This file is part of the Bos Oanda Client Bundle.
 * (c) Michael A. Bos <mab05k@gmail.com>
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Bos\OandaClient\Definition\Position;

use Bos\OandaClient\Definition\Traits\CommissionTrait;
use Bos\OandaClient\Definition\Traits\FinancingTrait;
use Bos\OandaClient\Definition\Traits\GuaranteedExecutionFeesTrait;
use Bos\OandaClient\Definition\Traits\InstrumentTrait;
use Bos\OandaClient\Definition\Traits\MarginUsedTrait;
use Bos\OandaClient\Definition\Traits\ProfitLossTrait;
use JMS\Serializer\Annotation as Serializer;

/**
 * Class Position.
 */
class Position
{
    use ProfitLossTrait;
    use GuaranteedExecutionFeesTrait;
    use MarginUsedTrait;
    use FinancingTrait;
    use CommissionTrait;
    use InstrumentTrait;

    /**
     * @var PositionSide|null
     *
     * @Serializer\SerializedName("long")
     * @Serializer\Type("Bos\OandaClient\Definition\Position\PositionSide")
     */
    private $long;

    /**
     * @var PositionSide|null
     *
     * @Serializer\SerializedName("short")
     * @Serializer\Type("Bos\OandaClient\Definition\Position\PositionSide")
     */
    private $short;

    /**
     * @return PositionSide|null
     */
    public function getLong(): ?PositionSide
    {
        return $this->long;
    }

    /**
     * @param PositionSide|null $long
     *
     * @return Position
     */
    public function setLong(?PositionSide $long): self
    {
        $this->long = $long;

        return $this;
    }

    /**
     * @return PositionSide|null
     */
    public function getShort(): ?PositionSide
    {
        return $this->short;
    }

    /**
     * @param PositionSide|null $short
     *
     * @return Position
     */
    public function setShort(?PositionSide $short): self
    {
        $this->short = $short;

        return $this;
    }
}
