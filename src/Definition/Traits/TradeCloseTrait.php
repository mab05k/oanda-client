<?php

/*
 * This file is part of the Bos Oanda Client Bundle.
 * (c) Michael A. Bos <mab05k@gmail.com>
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Bos\OandaClient\Definition\Traits;

use Bos\OandaClient\Definition\Transaction\MarketOrder\MarketOrderTradeClose;
use JMS\Serializer\Annotation as Serializer;

/**
 * Trait TradeCloseTrait.
 */
trait TradeCloseTrait
{
    /**
     * @var MarketOrderTradeClose|null
     *
     * @Serializer\SerializedName("tradeClose")
     * @Serializer\Type("Bos\OandaClient\Definition\Transaction\MarketOrder\MarketOrderTradeClose")
     */
    private $tradeClose;

    /**
     * @return MarketOrderTradeClose|null
     */
    public function getTradeClose(): ?MarketOrderTradeClose
    {
        return $this->tradeClose;
    }

    /**
     * @param MarketOrderTradeClose|null $tradeClose
     *
     * @return $this
     */
    public function setTradeClose(?MarketOrderTradeClose $tradeClose)
    {
        $this->tradeClose = $tradeClose;

        return $this;
    }
}
