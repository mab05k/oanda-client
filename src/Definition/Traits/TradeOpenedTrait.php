<?php

/*
 * This file is part of the Bos Oanda Client Bundle.
 * (c) Michael A. Bos <mab05k@gmail.com>
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Bos\OandaClient\Definition\Traits;

use Bos\OandaClient\Definition\Transaction\Trade\TradeOpened;
use JMS\Serializer\Annotation as Serializer;

/**
 * Trait TradeOpenedTrait.
 */
trait TradeOpenedTrait
{
    /**
     * @var TradeOpened|null
     *
     * @Serializer\SerializedName("tradeOpened")
     * @Serializer\Type("Bos\OandaClient\Definition\Transaction\Trade\TradeOpened")
     */
    private $tradeOpened;

    /**
     * @return TradeOpened|null
     */
    public function getTradeOpened(): ?TradeOpened
    {
        return $this->tradeOpened;
    }

    /**
     * @param TradeOpened|null $tradeOpened
     *
     * @return $this
     */
    public function setTradeOpened(?TradeOpened $tradeOpened)
    {
        $this->tradeOpened = $tradeOpened;

        return $this;
    }
}
