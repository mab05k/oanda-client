<?php

/*
 * This file is part of the Bos Oanda Client Bundle.
 * (c) Michael A. Bos <mab05k@gmail.com>
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Bos\OandaClient\Definition\Traits;

use Bos\OandaClient\Definition\Transaction\Trade\TradeClosed;
use JMS\Serializer\Annotation as Serializer;

/**
 * Trait TradesClosedTrait.
 */
trait TradesClosedTrait
{
    /**
     * @var array|TradeClosed[]
     *
     * @Serializer\SerializedName("tradesClosed")
     * @Serializer\Type("array<Bos\OandaClient\Definition\Transaction\Trade\TradeClosed>")
     */
    private $tradesClosed;

    /**
     * @return TradeClosed[]|array
     */
    public function getTradesClosed()
    {
        return $this->tradesClosed;
    }

    /**
     * @param TradeClosed[]|array $tradesClosed
     *
     * @return TradesClosedTrait
     */
    public function setTradesClosed($tradesClosed)
    {
        $this->tradesClosed = $tradesClosed;

        return $this;
    }

    /**
     * @return int|null
     */
    public function getFirstTradeClosedId(): ?int
    {
        $firstTradeClosed = $this->getFirstTradeClosed();
        if (null === $firstTradeClosed) {
            return null;
        }

        return (int) $firstTradeClosed->getTradeId();
    }

    /**
     * @return TradeClosed|null
     */
    public function getFirstTradeClosed(): ?TradeClosed
    {
        return $this->tradesClosed[0] ?? null;
    }
}
