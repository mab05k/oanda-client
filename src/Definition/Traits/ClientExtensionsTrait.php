<?php

/*
 * This file is part of the Bos Oanda Client Bundle.
 * (c) Michael A. Bos <mab05k@gmail.com>
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Bos\OandaClient\Definition\Traits;

use Bos\OandaClient\Definition\Transaction\ClientExtension\ClientExtension;
use JMS\Serializer\Annotation as Serializer;

/**
 * Trait ClientExtensionsTrait.
 *
 * @author Michael Bos <mab05k@gmail.com>
 */
trait ClientExtensionsTrait
{
    /**
     * @var ClientExtension|null
     *
     * @Serializer\SerializedName("clientExtensions")
     * @Serializer\Type("Bos\OandaClient\Definition\Transaction\ClientExtension\ClientExtension")
     */
    protected $clientExtensions;

    /**
     * @return ClientExtension|null
     */
    public function getClientExtensions()
    {
        return $this->clientExtensions;
    }

    /**
     * @param ClientExtension $clientExtensions
     *
     * @return $this
     */
    public function setClientExtensions(?ClientExtension $clientExtensions)
    {
        $this->clientExtensions = $clientExtensions;

        return $this;
    }
}
