<?php

/*
 * This file is part of the Bos Oanda Client Bundle.
 * (c) Michael A. Bos <mab05k@gmail.com>
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Bos\OandaClient\Definition\Traits;

use Brick\Money\Money;

/**
 * Class CommissionTrait.
 */
trait CommissionTrait
{
    /**
     * @var Money|null
     *
     * @Serializer\SerializedName("commission")
     * @Serializer\Type("Brick\Money\Money")
     */
    private $commission;

    /**
     * @return Money|null
     */
    public function getCommission(): ?Money
    {
        return $this->commission;
    }

    /**
     * @param Money|null $commission
     *
     * @return $this
     */
    public function setCommission(?Money $commission)
    {
        $this->commission = $commission;

        return $this;
    }
}
