<?php

/*
 * This file is part of the Bos Oanda Client Bundle.
 * (c) Michael A. Bos <mab05k@gmail.com>
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Bos\OandaClient\Definition\Traits;

use JMS\Serializer\Annotation as Serializer;

/**
 * Trait GoodUntilDateTrait.
 *
 * @author Michael Bos <mab05k@gmail.com>
 */
trait GoodUntilDateTrait
{
    /**
     * @var \DateTime|null
     *
     * @Serializer\SerializedName("gtdTime")
     * @Serializer\Type("DateTime")
     */
    private $gtdTime;

    /**
     * @return \DateTime|null
     */
    public function getGtdTime(): ?\DateTime
    {
        return $this->gtdTime;
    }

    /**
     * @param \DateTime|null $gtdTime
     *
     * @return $this
     */
    public function setGtdTime(?\DateTime $gtdTime)
    {
        $this->gtdTime = $gtdTime;

        return $this;
    }
}
