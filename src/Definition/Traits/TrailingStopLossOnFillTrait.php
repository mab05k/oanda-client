<?php

/*
 * This file is part of the Bos Oanda Client Bundle.
 * (c) Michael A. Bos <mab05k@gmail.com>
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Bos\OandaClient\Definition\Traits;

use Bos\OandaClient\Definition\Transaction\Detail\TrailingStopLossDetail;
use JMS\Serializer\Annotation as Serializer;

/**
 * Trait TrailingStopLossOnFillTrait.
 *
 * @author Michael Bos <mab05k@gmail.com>
 */
trait TrailingStopLossOnFillTrait
{
    /**
     * @var TrailingStopLossDetail|null
     *
     * @Serializer\SerializedName("trailingStopLossOnFill")
     * @Serializer\Type("Bos\OandaClient\Definition\Transaction\Detail\TrailingStopLossDetail")
     */
    private $trailingStopLossOnFill;

    /**
     * @return TrailingStopLossDetail|null
     */
    public function getTrailingStopLossOnFill(): ?TrailingStopLossDetail
    {
        return $this->trailingStopLossOnFill;
    }

    /**
     * @param TrailingStopLossDetail|null $trailingStopLossOnFill
     *
     * @return $this
     */
    public function setTrailingStopLossOnFill(?TrailingStopLossDetail $trailingStopLossOnFill)
    {
        $this->trailingStopLossOnFill = $trailingStopLossOnFill;

        return $this;
    }
}
