<?php

/*
 * This file is part of the Bos Oanda Client Bundle.
 * (c) Michael A. Bos <mab05k@gmail.com>
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Bos\OandaClient\Tests\Unit\Response\Order;

use Bos\OandaClient\Definition\Order\Order;
use Bos\OandaClient\Response\Order\OrderResponse;
use PHPUnit\Framework\TestCase;

class OrderResponseTest extends TestCase
{
    /**
     * @var OrderResponse
     */
    private $SUT;

    public function setUp(): void
    {
        $this->SUT = new OrderResponse();
    }

    public function testGettersSetters()
    {
        $order = new Order();
        $this->assertInstanceOf(OrderResponse::class, $this->SUT->setOrder($order));
        $this->assertInstanceOf(Order::class, $this->SUT->getOrder());
    }
}
