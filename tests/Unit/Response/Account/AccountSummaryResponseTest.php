<?php

/*
 * This file is part of the Bos Oanda Client Bundle.
 * (c) Michael A. Bos <mab05k@gmail.com>
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Bos\OandaClient\Tests\Unit\Response\Account;

use Bos\OandaClient\Definition\Account\AccountSummary;
use Bos\OandaClient\Response\Account\AccountSummaryResponse;
use PHPUnit\Framework\TestCase;

class AccountSummaryResponseTest extends TestCase
{
    /**
     * @var AccountSummaryResponse
     */
    private $SUT;

    public function setUp(): void
    {
        $this->SUT = new AccountSummaryResponse();
    }

    public function testGettersSetters()
    {
        $account = new AccountSummary();
        $this->assertInstanceOf(AccountSummaryResponse::class, $this->SUT->setAccount($account));
        $this->assertInstanceOf(AccountSummary::class, $this->SUT->getAccount());
    }
}
