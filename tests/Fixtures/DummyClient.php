<?php

/*
 * This file is part of the Bos Oanda Client Bundle.
 * (c) Michael A. Bos <mab05k@gmail.com>
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Bos\OandaClient\Tests\Fixtures;

use Bos\OandaClient\Client\AbstractOandaClient;
use Bos\OandaClient\DTO\Definition\Account\AccountCollection;

/**
 * Class DummyClient.
 */
class DummyClient extends AbstractOandaClient
{
    public function testCall()
    {
        $request = $this->createRequest('GET', '/accounts/%s'.sprintf('/test/%s', 'yes'));

        return $this->sendRequest($request, AccountCollection::class, 200);
    }
}
